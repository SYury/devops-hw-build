## Домашка по devops
Этот проект закрывает пункты 1 и 4 (build requirements + CI).
## Build requirements
Java 8

Maven 3.6.1

Остальные библиотеки Maven докачивает сам в процессе сборки, см. `project/pom.xml`.
