create table ip_data (
    ip VARCHAR(50),-- not null primary key,
    region VARCHAR(50)
);

create table user_data (
    ip VARCHAR(50),-- not null primary key,
    browser VARCHAR(50),
    gender BIT,
    age INT
);

create table user_log (
    ip VARCHAR(50),
    time TIMESTAMP,
    query VARCHAR(500),
    page_size INT,
    status INT,
    info VARCHAR(500)
);


