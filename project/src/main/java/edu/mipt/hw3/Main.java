package edu.mipt.hw3;

import edu.mipt.hw3.db.LoadDb;
import edu.mipt.hw3.download.YandexDiskDownloader;
import edu.mipt.hw3.parse.ParseException;
import edu.mipt.hw3.query.QueryProcessor;
import edu.mipt.hw3.query.QueryWriter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws SQLException, IOException, ParseException {
        if(args.length < 1){
            System.err.println("You must pass at least one argument");
            return;
        }
        switch (args[0]) {
            case "query1":
                List<Integer> data1 = new ArrayList<>();
                for(int i = 1; i < args.length; i++)
                    data1.add(Integer.parseInt(args[i]));
                new QueryWriter(new QueryProcessor(new LoadDb().loadDb())).writeFirstQuery(data1);
                break;
            case "query2":
                int times = 1;
                if(args.length > 1){
                    times = Integer.parseInt(args[1]);
                }
                new QueryWriter(new QueryProcessor(new LoadDb().loadDb())).writeSecondQuery(times);
                break;
            case "query3":
                List<String> data3 = new ArrayList<>();
                for(int i = 1; i < args.length; i++)
                    data3.add(args[i]);
                new QueryWriter(new QueryProcessor(new LoadDb().loadDb())).writeThirdQuery(data3);
                break;
            case "query4":
                List<int[]> data4 = new ArrayList<>();
                int prv = -1;
                for(int i = 1; i < args.length; i++){
                    if(i%2 == 1){
                        prv = Integer.parseInt(args[i]);
                    }
                    else{
                        data4.add(new int[]{prv, Integer.parseInt(args[i])});
                    }
                }
                new QueryWriter(new QueryProcessor(new LoadDb().loadDb())).writeFourthQuery(data4);
                break;
            case "query5":
                List<Integer> data5 = new ArrayList<>();
                for(int i = 1; i < args.length; i++)
                    data5.add(Integer.parseInt(args[i]));
                new QueryWriter(new QueryProcessor(new LoadDb().loadDb())).writeFifthQuery(data5);
                break;
            case "download":
                YandexDiskDownloader downloader = new YandexDiskDownloader();
                downloader.downloadFiles(new String[]{
                        "HSE-OOP-HW4-Data/ip_data_M/ipDataM.txt",
                        "HSE-OOP-HW4-Data/user_data_M/userDataM",
                        "HSE-OOP-HW4-Data/user_logs_M/logsLM.txt"});
                break;
            default:
                System.err.println("Unrecognized argument: " + args[0]);
        }
    }
}
