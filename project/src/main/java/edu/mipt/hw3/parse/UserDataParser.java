package edu.mipt.hw3.parse;

import edu.mipt.hw3.domain.UserData;

public class UserDataParser {
    public static UserData parse(String s) throws ParseException {
        String[] tokens = s.split("\t");
        if (tokens.length != 4)
            throw new ParseException("Parse error in UserDataParser: " + s);
        else
            return new UserData(tokens[0], tokens[1], tokens[2].equals("male"), Integer.parseInt(tokens[3]));
    }
}
