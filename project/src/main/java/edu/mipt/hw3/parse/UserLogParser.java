package edu.mipt.hw3.parse;

import edu.mipt.hw3.domain.UserLog;

import java.sql.Timestamp;

public class UserLogParser {
    private static Timestamp convertTime(String s){
        StringBuilder time = new StringBuilder();
        time.append(s, 0, 4);
        for(int i = 4; i <= 12; i += 2){
            if(i <= 6)
                time.append('-');
            else if(i >= 10)
                time.append(':');
            else
                time.append(' ');
            time.append(s, i, i + 2);
        }
        return Timestamp.valueOf(time.toString());
    }
    public static UserLog parse(String s) throws ParseException {
        String[] tokens = s.split("\t");
        if (tokens.length != 8)
            throw new ParseException("Parse error in UserLogParser");
        else
            return new UserLog(
                    tokens[0],
                    convertTime(tokens[3]),
                    tokens[4],
                    Integer.parseInt(tokens[5]),
                    Integer.parseInt(tokens[6]),
                    tokens[7]);
    }
}
