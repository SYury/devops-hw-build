package edu.mipt.hw3.parse;

import edu.mipt.hw3.domain.IpData;

public class IpDataParser {
    public static IpData parse(String s) throws ParseException {
        String[] tokens = s.split("\t");
        if (tokens.length != 2)
            throw new ParseException("Parse error in IpDataParser");
        else
            return new IpData(tokens[0], tokens[1]);
    }
}
