package edu.mipt.hw3.parse;

public class ParseException extends Exception{
    ParseException(String s){
        super(s);
    }
}
