package edu.mipt.hw3.query;

import edu.mipt.hw3.domain.UserData;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
public class QueryWriter {
    private final QueryProcessor processor;

    public void writeFirstQuery(Collection<Integer> days) throws SQLException, IOException {
        FileOutputStream fileOut = new FileOutputStream("query1.xls");
        XSSFWorkbook wb = new XSSFWorkbook();
        String[] columns = {"IP", "Browser", "Gender", "Age"};
        for(Integer day : days){
            List<UserData> res = processor.firstQuery(day);
            XSSFSheet sheet = wb.createSheet();
            XSSFRow headerRow = sheet.createRow(0);
            XSSFCellStyle headerStyle = createHeaderStyle(wb);
            for(int i = 0; i < columns.length; i++){
                XSSFCell cell = headerRow.createCell(i);
                cell.setCellStyle(headerStyle);
                cell.setCellValue(columns[i]);
            }
            sheet.createFreezePane(0, 1);
            for(int i = 0; i < res.size(); i++){
                UserData current = res.get(i);
                XSSFRow row = sheet.createRow(i + 1);
                XSSFCell cell = row.createCell(0);
                cell.setCellValue(current.getIp());
                cell = row.createCell(1);
                cell.setCellValue(current.getBrowser());
                cell = row.createCell(2);
                cell.setCellValue(current.isGender());
                cell = row.createCell(3);
                cell.setCellValue(current.getAge());
            }
        }
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    public void writeSecondQuery(int times) throws SQLException, IOException {
        FileOutputStream fileOut = new FileOutputStream("query2.xls");
        XSSFWorkbook wb = new XSSFWorkbook();
        for(int i = 0; i < times; i++){
            Set<String> res = processor.secondQuery();
            XSSFSheet sheet = wb.createSheet();
            XSSFRow headerRow = sheet.createRow(0);
            XSSFCellStyle headerStyle = createHeaderStyle(wb);
            XSSFCell headerCell = headerRow.createCell(0);
            headerCell.setCellStyle(headerStyle);
            headerCell.setCellValue("Regions");
            sheet.createFreezePane(0, 1);
            int id = 1;
            for(String s : res){
                XSSFRow row = sheet.createRow(id++);
                XSSFCell cell = row.createCell(0);
                cell.setCellValue(s);
            }
        }
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }


    public void writeThirdQuery(Collection<String> dates) throws SQLException, IOException {
        FileOutputStream fileOut = new FileOutputStream("query3.xls");
        XSSFWorkbook wb = new XSSFWorkbook();
        for(String date : dates){
            int[] res = processor.thirdQuery(date);
            XSSFSheet sheet = wb.createSheet();
            XSSFRow headerRow = sheet.createRow(0);
            XSSFCellStyle headerStyle = createHeaderStyle(wb);
            String[] columns = {"Male", "Female"};
            for(int i = 0; i < columns.length; i++) {
                XSSFCell headerCell = headerRow.createCell(i);
                headerCell.setCellStyle(headerStyle);
                headerCell.setCellValue(columns[i]);
            }
            sheet.createFreezePane(0, 1);
            XSSFRow dataRow = sheet.createRow(1);
            XSSFCell cell = dataRow.createCell(0);
            cell.setCellValue(res[0]);
            cell = dataRow.createCell(1);
            cell.setCellValue(res[1]);
        }
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    public void writeFourthQuery(Collection<int[]> data) throws SQLException, IOException {
        FileOutputStream fileOut = new FileOutputStream("query4.xls");
        XSSFWorkbook wb = new XSSFWorkbook();
        for(int[] query : data){
            List<String> res = processor.fourthQuery(query[0], query[1]);
            XSSFSheet sheet = wb.createSheet();
            XSSFRow headerRow = sheet.createRow(0);
            XSSFCellStyle headerStyle = createHeaderStyle(wb);
            XSSFCell headerCell = headerRow.createCell(0);
            headerCell.setCellStyle(headerStyle);
            headerCell.setCellValue("Resources");
            sheet.createFreezePane(0, 1);
            int id = 1;
            for(String s : res){
                XSSFRow row = sheet.createRow(id++);
                XSSFCell cell = row.createCell(0);
                cell.setCellValue(s);
            }
        }
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    public void writeFifthQuery(Collection<Integer> data) throws SQLException, IOException {
        FileOutputStream fileOut = new FileOutputStream("query5.xls");
        XSSFWorkbook wb = new XSSFWorkbook();
        for(Integer day : data){
            List<String> res = processor.fifthQuery(day);
            XSSFSheet sheet = wb.createSheet();
            XSSFRow headerRow = sheet.createRow(0);
            XSSFCellStyle headerStyle = createHeaderStyle(wb);
            XSSFCell headerCell = headerRow.createCell(0);
            headerCell.setCellStyle(headerStyle);
            headerCell.setCellValue("Regions");
            sheet.createFreezePane(0, 1);
            int id = 1;
            for(String s : res){
                XSSFRow row = sheet.createRow(id++);
                XSSFCell cell = row.createCell(0);
                cell.setCellValue(s);
            }
        }
        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    private static XSSFCellStyle createHeaderStyle(XSSFWorkbook wb){
        XSSFFont font = wb.createFont();
        font.setBold(true);
        XSSFCellStyle style = wb.createCellStyle();
        style.setFont(font);
        return style;
    }
}