package edu.mipt.hw3.query;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.domain.UserData;
import edu.mipt.hw3.domain.UserLog;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
public class QueryProcessor {
    private final ConnectionSource source;

    public List<UserData> firstQuery(int days) throws SQLException {
        String to = java.time.LocalDate.now().toString();
        String from = java.time.LocalDate.now().minusDays(days - 1).toString();
        return source.preparedStatement(
                "SELECT " +
                        "d.ip, browser, gender, age, " +
                        "COUNT(*) AS cnt " +
                        "FROM user_data d INNER JOIN user_log l ON d.ip = l.ip " +
                        "WHERE time BETWEEN " + "?" + " AND " + "?" + " GROUP BY (d.ip, browser, gender, age) ORDER BY cnt DESC LIMIT 10;",
                stmt -> {
                    stmt.setString(1, from + " 00:00:00");
                    stmt.setString(2, to + " 23:59:59");
                    List<UserData> out = new ArrayList<>();
                    ResultSet resultSet = stmt.executeQuery();
                    while (resultSet.next()) {
                        out.add(new UserData(
                            resultSet.getString("ip"),
                            resultSet.getString("browser"),
                            resultSet.getBoolean("gender"),
                            resultSet.getInt("age")));
                    }
                    return out;
        });
    }

    public Set<String> secondQuery() throws SQLException {
        return source.statement(stmt -> {
            Set<String> res = new HashSet<>();
            ResultSet resultSet0 = stmt.executeQuery("SELECT COUNT(*) AS CNT FROM ip_data d INNER JOIN user_log l ON d.ip = l.ip;");
            resultSet0.next();
            int nLogs = resultSet0.getInt("cnt");
            resultSet0 = stmt.executeQuery(
                    "SELECT COUNT(DISTINCT region) AS cnt FROM ip_data;"
            );
            resultSet0.next();
            int nRegions = resultSet0.getInt("cnt");
            double ratio = (double)nLogs/(double)nRegions;
            int lowerBound = (int)(Math.ceil(ratio) + 1e-9);
            ResultSet resultSet = stmt.executeQuery(
                    "SELECT DISTINCT region, " +
                            "COUNT(*) AS cnt " +
                            "FROM ip_data d INNER JOIN user_log l ON d.ip = l.ip GROUP BY region "+
                            "HAVING cnt" +
                            " >= "+String.valueOf(lowerBound)+";"
            );
            while (resultSet.next()) {
                res.add(resultSet.getString("region"));
            }
            return res;
        });
    }

    public int[] thirdQuery(String date) throws SQLException {
        return source.preparedStatement(
                "SELECT " +
                        "SUM( CASE WHEN gender = 1 THEN 1 ELSE 0 END) as male," +
                        "SUM (CASE WHEN gender = 0 THEN 1 ELSE 0 END) as female " +
                        "FROM user_data d INNER JOIN user_log l ON d.ip = l.ip " +
                        "WHERE time BETWEEN " + "?" + " AND " + "?" + ";",
                stmt -> {
                    stmt.setString(1, date + " 00:00:00");
                    stmt.setString(2, date + " 23:59:59");
                    ResultSet resultSet = stmt.executeQuery();
                    int[] answer = new int[2];
                    resultSet.next();
                    answer[0] = resultSet.getInt("male");
                    answer[1] = resultSet.getInt("female");
                    return answer;
        });
    }

    public List<String> fourthQuery(int age, int days) throws SQLException {
        String to = java.time.LocalDate.now().toString();
        String from = java.time.LocalDate.now().minusDays(days - 1).toString();
        return source.preparedStatement(
                "SELECT " +
                        "REPLACE(REGEXP_REPLACE(REPLACE(query, '//', ' '), '/.*$', ''), ' ', '//') AS resource, COUNT(*) AS cnt " +
                        "FROM (SELECT * FROM user_log WHERE time BETWEEN " + "?" + " AND " + "?" +
                        ") l INNER JOIN (SELECT * FROM user_data WHERE age < " + "?" +
                        ") d ON l.ip = d.ip GROUP BY resource ORDER BY cnt DESC;",
                stmt -> {
                    stmt.setString(1, from + " 00:00:00");
                    stmt.setString(2, to + " 23:59:59");
                    stmt.setInt(3, age);
                    ResultSet resultSet = stmt.executeQuery();
                    List<String> res = new ArrayList<>();
                    while (resultSet.next()) {
                        res.add(resultSet.getString("resource"));
                    }
                    return res;
        });
    }

    public List<String> fifthQuery(int days) throws SQLException {
        String to = java.time.LocalDate.now().toString();
        String from = java.time.LocalDate.now().minusDays(days - 1).toString();
        return source.preparedStatement(
                "SELECT " +
                        "region, COUNT(*) AS cnt " +
                        "FROM ip_data p INNER JOIN (SELECT * FROM user_log WHERE (time BETWEEN " +
                        "?" + " AND " + "?" +
                        ") AND (query LIKE 'http://lenta.ru/%' OR query LIKE 'http://lenta.ru')) l ON l.ip = p.ip GROUP BY region ORDER BY cnt DESC;",
                stmt -> {
                    stmt.setString(1, from + " 00:00:00");
                    stmt.setString(2, to + " 23:59:59");
                    ResultSet resultSet = stmt.executeQuery();
                    List<String> res = new ArrayList<>();
                    while (resultSet.next()) {
                        res.add(resultSet.getString("region"));
                    }
                    return res;
        });
    }

    public void sixthQuery(UserLog log) throws SQLException {
        source.preparedStatement("INSERT INTO user_log(ip, time, query, page_size, status, info) VALUES (?, ?, ?, ?, ?, ?)", insertUserLog -> {
            insertUserLog.setString(1, log.getIp());
            insertUserLog.setTimestamp(2, log.getTime());
            insertUserLog.setString(3, log.getQuery());
            insertUserLog.setInt(4, log.getPageSize());
            insertUserLog.setInt(5, log.getStatus());
            insertUserLog.setString(6, log.getInfo());
            insertUserLog.execute();
        });
    }

    public void seventhQuery(String date) throws SQLException {
        Timestamp time = Timestamp.valueOf(date + " 00:00:00");
        source.preparedStatement("DELETE FROM user_log WHERE time < (?);", removeLogs -> {
            removeLogs.setTimestamp(1, time);
            removeLogs.execute();
        });
    }

    public void eighthQuery(String date) throws SQLException {
        Timestamp time = Timestamp.valueOf(date + " 00:00:00");
        source.preparedStatement("UPDATE user_log SET query=REPLACE(query, '.ru/', '.com/') WHERE time >= (?)", updateLogs -> {
            updateLogs.setTimestamp(1, time);
            updateLogs.execute();
        });
    }

}
