package edu.mipt.hw3.download;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Properties;

public class YandexDiskDownloader implements Downloader {

    private HttpClient httpClient = HttpClients.createSystem();
    private CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

    private final static String DISK_URL = "https://webdav.yandex.ru";

    public YandexDiskDownloader() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/main/resources/edu/mipt/hw3/yadisk.properties"));
        String login = properties.getProperty("login");
        String password = properties.getProperty("password");
        Credentials credentials = new UsernamePasswordCredentials(login, password);
        credentialsProvider.setCredentials(AuthScope.ANY, credentials);
    }

    public InputStream doGet(String path) throws IOException {
        HttpGet request = null;
        try{
            URIBuilder builder = new URIBuilder(DISK_URL);
            builder.setPath("/"+path);
            request = new HttpGet(builder.build());
        } catch (URISyntaxException e){
            e.printStackTrace();
        }
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credentialsProvider);
        HttpResponse response = httpClient.execute(request, context);
        int code = response.getStatusLine().getStatusCode();
        int codeClass = code / 100;
        if(codeClass==4 || codeClass==5){
            return null;
        }
        return response.getEntity().getContent();
    }

    public void downloadFile(String path) throws IOException {
        InputStream stream = doGet(path);
        String contents = IOUtils.toString(stream, "UTF-8");

        String savePath = path.replace('/', '_');
        PrintWriter writer = new PrintWriter(savePath);
        writer.print(contents);
        writer.flush();
    }

    public void downloadFiles(String[] paths) throws IOException {
        for(String path : paths){
            downloadFile(path);
        }
    }
}
