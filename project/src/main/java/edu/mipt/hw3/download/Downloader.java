package edu.mipt.hw3.download;

import java.io.IOException;

public interface Downloader {
    void downloadFile(String path) throws IOException;
    void downloadFiles(String[] paths) throws IOException;
}
