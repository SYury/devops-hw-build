package edu.mipt.hw3.domain;

import lombok.Data;

@Data
public class IpData {
    private final String ip;
    private final String region;
}
