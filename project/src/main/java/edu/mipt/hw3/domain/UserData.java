package edu.mipt.hw3.domain;

import lombok.Data;

@Data
public class UserData {
    private final String ip;
    private final String browser;
    private final boolean gender;
    private final int age;
}
