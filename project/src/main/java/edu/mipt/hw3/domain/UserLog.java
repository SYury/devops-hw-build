package edu.mipt.hw3.domain;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class UserLog {
    private final String ip;
    private final Timestamp time;
    private final String query;
    private final int pageSize;
    private final int status;
    private final String info;
}
