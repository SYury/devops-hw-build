package edu.mipt.hw3.dao;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.domain.UserData;

import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@AllArgsConstructor
public class UserDataDao {
    private final ConnectionSource source;

    private UserData createUserData(ResultSet resultSet) throws SQLException {
        return new UserData(
                resultSet.getString("ip"),
                resultSet.getString("browser"),
                resultSet.getBoolean("gender"),
                resultSet.getInt("age")
        );
    }

    public void saveUserData(Collection<UserData> userdata) throws SQLException {
        source.preparedStatement("insert into user_data(ip, browser, gender, age) values (?, ?, ?, ?)", insertUserData -> {
            for (UserData user : userdata) {
                insertUserData.setString(1, user.getIp());
                insertUserData.setString(2, user.getBrowser());
                insertUserData.setBoolean(3, user.isGender());
                insertUserData.setInt(4, user.getAge());
                insertUserData.execute();
            }
        });
    }

    public Set<UserData> getUserData() throws SQLException {
        return source.statement(stmt -> {
            Set<UserData> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from user_data");
            while (resultSet.next()) {
                result.add(createUserData(resultSet));
            }
            return result;
        });
    }
}
