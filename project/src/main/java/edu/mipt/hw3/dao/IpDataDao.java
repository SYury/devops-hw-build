package edu.mipt.hw3.dao;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.domain.IpData;

import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@AllArgsConstructor
public class IpDataDao {
    private final ConnectionSource source;

    private IpData createIpData(ResultSet resultSet) throws SQLException {
        return new IpData(
                resultSet.getString("ip"),
                resultSet.getString("region")
        );
    }

    public void saveIpData(Collection<IpData> ipdata) throws SQLException {
        source.preparedStatement("insert into ip_data(ip, region) values (?, ?)", insertIpData -> {
            for (IpData ip : ipdata) {
                insertIpData.setString(1, ip.getIp());
                insertIpData.setString(2, ip.getRegion());
                insertIpData.execute();
            }
        });
    }

    public Set<IpData> getIpData() throws SQLException {
        return source.statement(stmt -> {
            Set<IpData> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from ip_data");
            while (resultSet.next()) {
                result.add(createIpData(resultSet));
            }
            return result;
        });
    }
}
