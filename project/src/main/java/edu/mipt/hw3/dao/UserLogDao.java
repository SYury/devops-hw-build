package edu.mipt.hw3.dao;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.domain.UserLog;

import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@AllArgsConstructor
public class UserLogDao {
    private final ConnectionSource source;

    private UserLog createUserLog(ResultSet resultSet) throws SQLException {
        return new UserLog(
                resultSet.getString("ip"),
                resultSet.getTimestamp("time"),
                resultSet.getString("query"),
                resultSet.getInt("page_size"),
                resultSet.getInt("status"),
                resultSet.getString("info")
        );
    }

    public void saveUserLog(Collection<UserLog> userlogs) throws SQLException {
        source.preparedStatement("insert into user_log(ip, time, query, page_size, status, info) values (?, ?, ?, ?, ?, ?)", insertUserLog -> {
            for (UserLog log : userlogs) {
                insertUserLog.setString(1, log.getIp());
                insertUserLog.setTimestamp(2, log.getTime());
                insertUserLog.setString(3, log.getQuery());
                insertUserLog.setInt(4, log.getPageSize());
                insertUserLog.setInt(5, log.getStatus());
                insertUserLog.setString(6, log.getInfo());
                insertUserLog.execute();
            }
        });
    }

    public Set<UserLog> getUserLog() throws SQLException {
        return source.statement(stmt -> {
            Set<UserLog> result = new HashSet<>();
            ResultSet resultSet = stmt.executeQuery("select * from user_log");
            while (resultSet.next()) {
                result.add(createUserLog(resultSet));
            }
            return result;
        });
    }
}
