package edu.mipt.hw3.db;

import edu.mipt.hw3.dao.IpDataDao;
import edu.mipt.hw3.dao.UserDataDao;
import edu.mipt.hw3.dao.UserLogDao;
import edu.mipt.hw3.domain.IpData;
import edu.mipt.hw3.domain.UserData;
import edu.mipt.hw3.domain.UserLog;
import edu.mipt.hw3.parse.IpDataParser;
import edu.mipt.hw3.parse.ParseException;
import edu.mipt.hw3.parse.UserDataParser;
import edu.mipt.hw3.parse.UserLogParser;
import org.h2.jdbcx.JdbcConnectionPool;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LoadDb {
    public static ConnectionSource loadDb() throws SQLException, IOException, ParseException {
        ConnectionSource source = new ConnectionSource (
                JdbcConnectionPool.create("jdbc:h2:mem:database;DB_CLOSE_DELAY=-1", "", ""));
        new DbInit(source).create();
        IpDataDao ipDataDao = new IpDataDao(source);
        UserDataDao userDataDao = new UserDataDao(source);
        UserLogDao userLogDao = new UserLogDao(source);
        ipDataDao.saveIpData(readIpData());
        userDataDao.saveUserData(readUserData());
        userLogDao.saveUserLog(readUserLogs());
        return source;
    }

    static List<IpData> readIpData() throws IOException, ParseException {
        List<String> data = Files.readAllLines(Paths.get("HSE-OOP-HW4-Data_ip_data_M_ipDataM.txt"));
        List<IpData> ipData = new ArrayList<>();
        for(String s : data){
            IpData item = new IpDataParser().parse(s);
            ipData.add(item);
        }
        return ipData;
    }

    static List<UserData> readUserData() throws IOException, ParseException {
        List<String> data = Files.readAllLines(Paths.get("HSE-OOP-HW4-Data_user_data_M_userDataM"));
        List<UserData> userData = new ArrayList<>();
        for(String s : data){
            UserData item = new UserDataParser().parse(s);
            userData.add(item);
        }
        return userData;
    }

    static List<UserLog> readUserLogs() throws IOException, ParseException {
        List<String> data = Files.readAllLines(Paths.get("HSE-OOP-HW4-Data_user_logs_M_logsLM.txt"));
        List<UserLog> userLog = new ArrayList<>();
        int id = 0;
        for(String s : data){
            UserLog item = new UserLogParser().parse(s);
            userLog.add(item);
        }
        return userLog;
    }
}
