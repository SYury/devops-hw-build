package edu.mipt.hw3.dao;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.db.DbInit;
import edu.mipt.hw3.domain.UserLog;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static edu.mipt.hw3.dao.TestData.*;
import static org.junit.jupiter.api.Assertions.*;

class UserLogDaoTest {

    private ConnectionSource source = new ConnectionSource (
            JdbcConnectionPool.create("jdbc:h2:mem:database;DB_CLOSE_DELAY=-1", "", ""));
    private UserLogDao dao =
            new UserLogDao(source);

    @BeforeEach
    void setupDB() throws IOException, SQLException {
        new DbInit(source).create();
    }

    @AfterEach
    void tearDownDB() throws SQLException {
        source.statement(stmt -> {
            stmt.execute("drop all objects;");
        });
    }

    private int getUserLogCount() throws SQLException {
        return source.statement(stmt -> {
            ResultSet resultSet = stmt.executeQuery("select count(*) from user_log");
            resultSet.next();
            return resultSet.getInt(1);
        });
    }

    private Collection<UserLog> getTestUserLogs() { return Arrays.asList(firstLog, secondLog, thirdLog, fourthLog); }

    @Test
    void saveUserLogs() throws SQLException {
        Collection<UserLog> testUserLogs = getTestUserLogs();
        assertEquals(0, getUserLogCount());
        dao.saveUserLog(testUserLogs);
        assertEquals(testUserLogs.size(), getUserLogCount());
    }

    @Test
    void getUserLogs() throws SQLException {
        Collection<UserLog> testUserLogs = getTestUserLogs();
        dao.saveUserLog(testUserLogs);
        Set<UserLog> userLogs = dao.getUserLog();
        assertNotSame(testUserLogs, userLogs);
        assertEquals(new HashSet<>(testUserLogs), userLogs);
    }
}
