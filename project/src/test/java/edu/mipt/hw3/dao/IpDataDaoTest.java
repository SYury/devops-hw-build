package edu.mipt.hw3.dao;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.db.DbInit;
import edu.mipt.hw3.domain.IpData;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static edu.mipt.hw3.dao.TestData.*;
import static org.junit.jupiter.api.Assertions.*;

class IpDataDaoTest {

    private ConnectionSource source = new ConnectionSource (
            JdbcConnectionPool.create("jdbc:h2:mem:database;DB_CLOSE_DELAY=-1", "", ""));
    private IpDataDao dao =
            new IpDataDao(source);

    @BeforeEach
    void setupDB() throws IOException, SQLException {
        new DbInit(source).create();
    }

    @AfterEach
    void tearDownDB() throws SQLException {
        source.statement(stmt -> {
            stmt.execute("drop all objects;");
        });
    }

    private int getIpDataCount() throws SQLException {
        return source.statement(stmt -> {
            ResultSet resultSet = stmt.executeQuery("select count(*) from ip_data");
            resultSet.next();
            return resultSet.getInt(1);
        });
    }

    private Collection<IpData> getTestIpData() { return Arrays.asList(firstIp, secondIp, thirdIp); }

    @Test
    void saveIpData() throws SQLException {
        Collection<IpData> testIpData = getTestIpData();
        assertEquals(0, getIpDataCount());
        dao.saveIpData(testIpData);
        assertEquals(testIpData.size(), getIpDataCount());
    }

    @Test
    void getIpData() throws SQLException {
        Collection<IpData> testIpData = getTestIpData();
        dao.saveIpData(testIpData);
        Set<IpData> ipData = dao.getIpData();
        assertNotSame(testIpData, ipData);
        assertEquals(new HashSet<>(testIpData), ipData);
    }
}
