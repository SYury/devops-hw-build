package edu.mipt.hw3.dao;

import edu.mipt.hw3.domain.IpData;
import edu.mipt.hw3.domain.UserData;
import edu.mipt.hw3.domain.UserLog;

import java.sql.Timestamp;

public class TestData {
    public final static IpData firstIp = new IpData("46.39.245.204", "Russia");
    public final static IpData secondIp = new IpData("169.57.1.85", "Mexico");
    public final static IpData thirdIp = new IpData("185.108.141.49", "Bulgaria");

    public final static UserData firstUser = new UserData("46.39.245.204", "Firefox", true, 20);
    public final static UserData secondUser = new UserData("169.57.1.85", "Firefox", false, 30);
    public final static UserData thirdUser = new UserData("185.108.141.49", "Chrome", true, 22);

    public final static UserLog firstLog = new UserLog("169.57.1.85", new Timestamp(222222222), "some query", 0, 404, "some info");
    public final static UserLog secondLog = new UserLog("46.39.245.204", new Timestamp(14881488), "another query", 0, 403, "some info");
    public final static UserLog thirdLog = new UserLog("185.108.141.49", new Timestamp(222222333), "some query", 0, 502, "some info");
    public final static UserLog fourthLog = new UserLog("46.39.245.204", new Timestamp(1111110101), "yet another query", 100, 0, "no info");
}
