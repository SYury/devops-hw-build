package edu.mipt.hw3.dao;

import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.db.DbInit;
import edu.mipt.hw3.domain.UserData;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static edu.mipt.hw3.dao.TestData.*;
import static org.junit.jupiter.api.Assertions.*;

class UserDataDaoTest {

    private ConnectionSource source = new ConnectionSource (
            JdbcConnectionPool.create("jdbc:h2:mem:database;DB_CLOSE_DELAY=-1", "", ""));
    private UserDataDao dao =
            new UserDataDao(source);

    @BeforeEach
    void setupDB() throws IOException, SQLException {
        new DbInit(source).create();
    }

    @AfterEach
    void tearDownDB() throws SQLException {
        source.statement(stmt -> {
            stmt.execute("drop all objects;");
        });
    }

    private int getUserDataCount() throws SQLException {
        return source.statement(stmt -> {
            ResultSet resultSet = stmt.executeQuery("select count(*) from user_data");
            resultSet.next();
            return resultSet.getInt(1);
        });
    }

    private Collection<UserData> getTestUserData() { return Arrays.asList(firstUser, secondUser, thirdUser); }

    @Test
    void saveUserData() throws SQLException {
        Collection<UserData> testUserData = getTestUserData();
        assertEquals(0, getUserDataCount());
        dao.saveUserData(testUserData);
        assertEquals(testUserData.size(), getUserDataCount());
    }

    @Test
    void getUserData() throws SQLException {
        Collection<UserData> testUserData = getTestUserData();
        dao.saveUserData(testUserData);
        Set<UserData> userData = dao.getUserData();
        assertNotSame(testUserData, userData);
        assertEquals(new HashSet<>(testUserData), userData);
    }
}
