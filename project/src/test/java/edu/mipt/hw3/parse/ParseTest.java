package edu.mipt.hw3.parse;

import edu.mipt.hw3.domain.IpData;
import edu.mipt.hw3.domain.UserData;
import edu.mipt.hw3.domain.UserLog;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;

class ParseTest {
    @Test
    void parseIpData() throws ParseException {
        String s = "110.91.102.196\tChelyabinsk Oblast";
        IpData parsed = new IpDataParser().parse(s);
        assertEquals(parsed, new IpData("110.91.102.196", "Chelyabinsk Oblast"));
    }

    @Test
    void parseUserData() throws ParseException {
        String s = "56.167.169.126\tOpera/12.0\tmale\t33";
        UserData parsed = new UserDataParser().parse(s);
        assertEquals(parsed, new UserData("56.167.169.126", "Opera/12.0", true, 33));
    }

    @Test
    void parseUserLog() throws ParseException {
        String s = "33.49.147.163\t\t\t20140101014611\thttp://news.rambler.ru/3105700\t378\t431\tSafari/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729;) n";
        UserLog parsed = new UserLogParser().parse(s);
        assertEquals(parsed, new UserLog(
                "33.49.147.163",
                Timestamp.valueOf("2014-01-01 01:46:11"),
                "http://news.rambler.ru/3105700",
                378,
                431,
                "Safari/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729;) n"
        ));
    }
}
