package edu.mipt.hw3.query;

import edu.mipt.hw3.dao.IpDataDao;
import edu.mipt.hw3.dao.UserDataDao;
import edu.mipt.hw3.dao.UserLogDao;
import edu.mipt.hw3.db.ConnectionSource;
import edu.mipt.hw3.db.DbInit;
import edu.mipt.hw3.domain.IpData;
import edu.mipt.hw3.domain.UserData;
import edu.mipt.hw3.domain.UserLog;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class QueryTest {
    private ConnectionSource source = new ConnectionSource (
            JdbcConnectionPool.create("jdbc:h2:mem:database;DB_CLOSE_DELAY=-1", "", ""));
    private UserDataDao userDataDao = new UserDataDao(source);
    private UserLogDao userLogDao = new UserLogDao(source);
    private IpDataDao ipDataDao = new IpDataDao(source);

    @BeforeEach
    void setupDB() throws IOException, SQLException {
        new DbInit(source).create();
    }

    @AfterEach
    void tearDownDB() throws SQLException {
        source.statement(stmt -> {
            stmt.execute("drop all objects;");
        });
    }

    @Test
    void testFirstQuery() throws SQLException {
        UserData[] users = {
                new UserData("1.1.1.1", "Firefox", true, 30),
                new UserData("1.1.1.2", "Firefox", true, 30),
                new UserData("1.1.1.3", "Firefox", true, 30),
                new UserData("1.1.1.4", "Firefox", true, 30),
                new UserData("1.1.1.5", "Firefox", true, 30),
                new UserData("1.1.1.6", "Firefox", true, 30),
                new UserData("1.1.1.7", "Firefox", true, 30),
                new UserData("1.1.1.8", "Firefox", true, 30),
                new UserData("1.1.1.9", "Firefox", true, 30),
                new UserData("1.1.1.10", "Firefox", true, 30),
                new UserData("1.1.1.11", "Firefox", true, 30),
                new UserData("1.1.1.12", "Firefox", true, 30),
        };
        List<UserLog> logs = new ArrayList<>();
        String defaultTime = java.time.LocalDate.now().toString() + " 01:01:01";
        for(int i = 2; i < 12; i++){
            for(int j = 0; j < 2; j++){
                logs.add(new UserLog(
                        "1.1.1." + String.valueOf(i + 1),
                        Timestamp.valueOf(defaultTime),
                        "some query",
                        0,
                        404,
                        "some info"
                ));
            }
        }
        logs.add(new UserLog(
                "1.1.1.1",
                Timestamp.valueOf(defaultTime),
                "some query",
                0,
                404,
                "some info"
        ));
        String oldTime = java.time.LocalDate.now().minusDays(4).toString() + " 23:59:59";
        for(int j = 0; j < 3; j++){
            logs.add(new UserLog(
                 "1.1.1.2",
                 Timestamp.valueOf(oldTime),
                 "some query",
                 0,
                 404,
                 "some info"
            ));
        }
        HashSet<UserData> data = new HashSet<>();
        for(int i = 2; i < 12; i++){
            data.add(users[i]);
        }
        userLogDao.saveUserLog(logs);
        userDataDao.saveUserData(Arrays.asList(users));
        QueryProcessor q = new QueryProcessor(source);
        HashSet<UserData> res = new HashSet<>(q.firstQuery(3));
        assertEquals(data, res);
    }

    @Test
    void testSecondQuery() throws SQLException {
        IpData[] ips = {
                new IpData("1.1.1.1", "Moscow"),
                new IpData("1.1.1.2", "Not Moscow")
        };
        UserLog[] logs = {
                new UserLog(
                        "1.1.1.1",
                        Timestamp.valueOf("2014-01-01 00:00:02"),
                        "some query",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.2",
                        Timestamp.valueOf("2014-01-01 00:00:02"),
                        "some query",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.2",
                        Timestamp.valueOf("2014-01-01 00:00:02"),
                        "some query",
                        0,
                        404,
                        "some info"
                )
        };
        Set<String> data = new HashSet<>();
        data.add("Not Moscow");
        ipDataDao.saveIpData(Arrays.asList(ips));
        userLogDao.saveUserLog(Arrays.asList(logs));
        QueryProcessor q = new QueryProcessor(source);
        Set<String> res = q.secondQuery();
        assertEquals(data, res);
    }

    @Test
    void testThirdQuery() throws SQLException {
        UserData user = new UserData(
                "1.1.1.1",
                "Firefox",
                true,
                30
        );
        UserLog log = new UserLog(
                "1.1.1.1",
                Timestamp.valueOf("2019-01-01 02:02:02"),
                "some query",
                0,
                404,
                "some info"
        );
        userDataDao.saveUserData(Arrays.asList(user));
        userLogDao.saveUserLog(Arrays.asList(log));
        QueryProcessor q = new QueryProcessor(source);
        int[] result = q.thirdQuery("2019-01-01");
        assertEquals(result[0], 1);
        assertEquals(result[1], 0);
    }

    @Test
    void testFourthQuery() throws SQLException {
        UserData[] users = {
                new UserData("1.1.1.1", "Firefox", true, 30),
                new UserData("1.1.1.2", "Firefox", true, 12),
                new UserData("1.1.1.3", "Firefox", true, 30),
                new UserData("1.1.1.4", "Firefox", true, 30),
                new UserData("1.1.1.5", "Firefox", true, 30),
                new UserData("1.1.1.6", "Firefox", true, 30),
                new UserData("1.1.1.7", "Firefox", true, 30),
                new UserData("1.1.1.8", "Firefox", true, 30),
                new UserData("1.1.1.9", "Firefox", true, 30),
                new UserData("1.1.1.10", "Firefox", true, 15),
                new UserData("1.1.1.11", "Firefox", true, 16),
                new UserData("1.1.1.12", "Firefox", true, 30),
        };
        Timestamp current = Timestamp.valueOf(java.time.LocalDate.now().toString() + " 01:01:01");
        UserLog[] logs = {
                new UserLog(
                        "1.1.1.1",
                        current,
                        "http://a/1",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.2",
                        current,
                        "http://b/2",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.2",
                        current,
                        "http://c/3",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.10",
                        current,
                        "http://b/4",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.11",
                        current,
                        "http://a/5",
                        0,
                        404,
                        "some info"
                )
        };
        userLogDao.saveUserLog(Arrays.asList(logs));
        userDataDao.saveUserData(Arrays.asList(users));
        QueryProcessor q = new QueryProcessor(source);
        List<String> res = q.fourthQuery(16, 1);
        List<String> data = new ArrayList<>(Arrays.asList("http://b", "http://c"));
        assertEquals(data, res);
    }

    @Test
    public void testFifthQuery() throws SQLException {
        IpData[] ips = {
                new IpData("1.1.1.1", "Moscow"),
                new IpData("1.1.1.2", "Not Moscow")
        };
        Timestamp current = Timestamp.valueOf(java.time.LocalDate.now().toString() + " 01:01:01");
        UserLog[] logs = {
                new UserLog(
                        "1.1.1.1",
                        current,
                        "http://lenta.ru/1",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.2",
                        current,
                        "http://lenta.ru/2",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.2",
                        current,
                        "http://lenta.ru/2",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.1",
                        current,
                        "http://notlenta.ru/1",
                        0,
                        404,
                        "some info"
                ),
                new UserLog(
                        "1.1.1.1",
                        current,
                        "http://notlenta.ru/1",
                        0,
                        404,
                        "some info"
                )
        };
        userLogDao.saveUserLog(Arrays.asList(logs));
        ipDataDao.saveIpData(Arrays.asList(ips));
        QueryProcessor q = new QueryProcessor(source);
        List<String> res = q.fifthQuery(1);
        List<String> data = new ArrayList<>(Arrays.asList("Not Moscow", "Moscow"));
        assertEquals(data, res);
    }

    @Test
    public void testSixthQuery() throws SQLException {
        Timestamp current = Timestamp.valueOf(java.time.LocalDate.now().toString() + " 01:01:01");
        UserLog log1 = new UserLog(
                "1.1.1.1",
                current,
                "http://lenta.ru/1",
                0,
                404,
                "some info"
        );
        UserLog log2 = new UserLog(
                "1.1.1.1",
                current,
                "http://lenta.ru/1",
                0,
                404,
                "some info"
        );
        userLogDao.saveUserLog(Arrays.asList(log1));
        QueryProcessor q = new QueryProcessor(source);
        q.sixthQuery(log2);
        int size =
                source.statement(stmt -> {
                    ResultSet resultSet = stmt.executeQuery("select count(*) from user_log");
                    resultSet.next();
                    return resultSet.getInt(1);
                });
        assertEquals(2, size);
    }

    @Test
    public void seventhQueryTest() throws SQLException {
        Timestamp current = Timestamp.valueOf(java.time.LocalDate.now().toString() + " 01:01:01");
        Timestamp old = Timestamp.valueOf("2019-01-01 00:00:00");
        UserLog log1 = new UserLog(
                "1.1.1.1",
                current,
                "http://lenta.ru/1",
                0,
                404,
                "some info"
        );
        UserLog log2 = new UserLog(
                "1.1.1.1",
                old,
                "http://lenta.ru/1",
                0,
                404,
                "some info"
        );
        userLogDao.saveUserLog(Arrays.asList(log1, log2));
        QueryProcessor q = new QueryProcessor(source);
        q.seventhQuery("2019-01-02");
        int size =
                source.statement(stmt -> {
                    ResultSet resultSet = stmt.executeQuery("select count(*) from user_log");
                    resultSet.next();
                    return resultSet.getInt(1);
                });
        assertEquals(1, size);
    }

    @Test
    public void eighthQueryTest() throws SQLException {
        Timestamp current = Timestamp.valueOf(java.time.LocalDate.now().toString() + " 01:01:01");
        Timestamp old = Timestamp.valueOf("2019-01-01 00:00:00");
        UserLog log1 = new UserLog(
                "1.1.1.1",
                current,
                "http://lenta.ru/1",
                0,
                404,
                "some info"
        );
        UserLog log2 = new UserLog(
                "1.1.1.1",
                old,
                "http://lenta.ru/1",
                0,
                404,
                "some info"
        );
        userLogDao.saveUserLog(Arrays.asList(log1, log2));
        QueryProcessor q = new QueryProcessor(source);
        q.eighthQuery("2019-01-02");
        Set<UserLog> res = userLogDao.getUserLog();
        Set<UserLog> data = new HashSet<>(Arrays.asList(
                log2,
                new UserLog(
                        "1.1.1.1",
                        current,
                        "http://lenta.com/1",
                        0,
                        404,
                        "some info"
                )
        ));
        assertEquals(data, res);
    }
}
